<?php

use yii\db\Migration;

/**
 * Class m151005_165042_update_columns_to_comment_table
 */
class m151005_165042_update_columns_to_comment_table extends Migration
{
    protected $table = '{{%comment}}';
    
    /**
     * @return bool|void
     */
    public function safeUp()
    {
        $this->renameColumn($this->table, 'fullname', 'name');
        $this->renameColumn($this->table, 'social_account', 'fb_account');
        $this->addColumn($this->table, 'tw_account', $this->string());
        $this->addColumn($this->table, 'google_account', $this->string());
        $this->addColumn($this->table, 'vk_account', $this->string());
        $this->addColumn($this->table, 'sirname', $this->string());
        $this->addColumn($this->table, 'middle_name', $this->string());
    }
    
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn($this->table, 'name', 'fullname');
        $this->renameColumn($this->table, 'fb_account', 'social_account');
        $this->dropColumn($this->table, 'tw_account');
        $this->dropColumn($this->table, 'google_account');
        $this->dropColumn($this->table, 'vk_account');
        $this->dropColumn($this->table, 'sirname');
        $this->dropColumn($this->table, 'middle_name');
    }
}