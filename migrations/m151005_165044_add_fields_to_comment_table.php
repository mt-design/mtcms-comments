<?php

use yii\db\Migration;

/**
 * Class m151005_165044_add_fields_to_comment_table
 */
class m151005_165044_add_fields_to_comment_table extends Migration
{
    protected $table = '{{%comment}}';

    /**
     * @return bool|void
     */
    public function safeUp()
    {
        $this->addColumn($this->table, 'imageName', $this->string());
        $this->addColumn($this->table, 'imagePath', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'imagePath');
        $this->dropColumn($this->table, 'imageName');
    }
}