<?php

use yii\db\Migration;

/**
 * Class m151005_165045_rename_sirname_column_in_comment_table
 */
class m151005_165045_rename_sirname_column_in_comment_table extends Migration
{
    protected $table = '{{%comment}}';

    /**
     * @return bool|void
     */
    public function safeUp()
    {
        $this->renameColumn($this->table, 'sirname', 'surname');
        $model = \rmrevin\yii\module\Comments\models\AttributeSettings::find()->where(['attribute_value' => 'sirname'])->one();
        if (!empty($model)) {
            $model->attribute_value = 'surname';
            $model->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn($this->table, 'surname', 'sirname');
    }
}