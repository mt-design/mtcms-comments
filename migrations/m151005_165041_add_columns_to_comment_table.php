<?php

use yii\db\Migration;

/**
 * Class m151005_165041_add_columns_to_comment_table
 */
class m151005_165041_add_columns_to_comment_table extends Migration
{
    protected $table = '{{%comment}}';
    
    /**
     * @return bool|void
     */
    public function safeUp()
    {
        $this->addColumn($this->table, 'fullname', $this->string());
        $this->addColumn($this->table, 'specialty', $this->string());
        $this->addColumn($this->table, 'city', $this->string());
        $this->addColumn($this->table, 'age', $this->integer());
        $this->addColumn($this->table, 'email', $this->string());
        $this->addColumn($this->table, 'userphone', $this->string());
        $this->addColumn($this->table, 'title', $this->string());
        $this->addColumn($this->table, 'social_account', $this->text());
        $this->addColumn($this->table, 'ip', $this->integer());
    }
    
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, 'ip');
        $this->dropColumn($this->table, 'social_account');
        $this->dropColumn($this->table, 'title');
        $this->dropColumn($this->table, 'userphone');
        $this->dropColumn($this->table, 'email');
        $this->dropColumn($this->table, 'age');
        $this->dropColumn($this->table, 'city');
        $this->dropColumn($this->table, 'specialty');
        $this->dropColumn($this->table, 'fullname');
    }
}