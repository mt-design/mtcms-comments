<?php

use yii\db\Migration;

/**
 * Class m151005_165043_create_attribute_settings_table
 */
class m151005_165043_create_attribute_settings_table extends Migration
{
    protected $table = '{{%attribute_settings}}';
    
    /**
     * @return bool|void
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable($this->table, [
            'id'              => $this->primaryKey(),
            'attribute_value' => $this->string(),
            'attribute_name'  => $this->string(),
            'visible'         => $this->string(),
            'require'         => $this->string(),
        ], $tableOptions);
        
        $this->batchInsert($this->table, [
            'attribute_value',
            'attribute_name',
            'visible',
            'require',
        ], [
            ['title', 'Заголовок', '0', '0'],
            ['from', 'Автор', '0', '0'],
            ['created_by', 'Кем создано', '0', '0'],
            ['updated_by', 'Кем обновлено', '0', '0'],
            ['name', 'Имя', '0', '0'],
            ['sirname', 'Фамилия', '0', '0'],
            ['middle_name', 'Отчество', '0', '0'],
            ['age', 'Возраст', '0', '0'],
            ['userphone', 'Телефон', '0', '0'],
            ['email', 'Email', '0', '0'],
            ['city', 'Город', '0', '0'],
            ['specialty', 'Специальность', '0', '0'],
            ['fb_account', 'Фейсбук аккаунт', '0', '0'],
            ['tw_account', 'Твиттер аккаунт', '0', '0'],
            ['inst_account', 'Инстаграм аккаунт', '0', '0'],
            ['vk_account', 'Вконтакте аккаунт', '0', '0'],
        ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}