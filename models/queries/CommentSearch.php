<?php

namespace rmrevin\yii\module\Comments\models\queries;


use rmrevin\yii\module\Comments\models\Comment;
use yii\data\ActiveDataProvider;
use yii\base\Model;

class CommentSearch extends Comment
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                ['ip', 'age'],
                'integer'
            ],
            [
                [
                    'fb_account',
                    'tw_account',
                    'inst_account',
                    'vk_account',
                    'title',
                    'userphone',
                    'email',
                    'city',
                    'specialty',
                    'name',
                    'surname',
                    'middle_name',
                ],
                'safe'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Comment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'       => [
                'defaultOrder' => ['id' => SORT_DESC,],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'         => $this->id,
            'ip'         => $this->ip,
            'age'        => $this->age,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'fb_account', $this->fb_account])
            ->andFilterWhere(['like', 'tw_account', $this->tw_account])
            ->andFilterWhere(['like', 'inst_account', $this->inst_account])
            ->andFilterWhere(['like', 'vk_account', $this->vk_account])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'userphone', $this->userphone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'specialty', $this->specialty])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'middle_name', $this->middle_name]);

        return $dataProvider;
    }
}