<?php

namespace rmrevin\yii\module\Comments\models;

use Yii;

/**
 * This is the model class for table "{{%attribute_settings}}".
 *
 * @property int $id
 * @property string $attribute_value
 * @property string $attribute_name
 * @property string $visible
 * @property string $require
 *
 */
class AttributeSettings extends \yii\db\ActiveRecord
{
    const VISIBLE_TRUE = '1';
    const REQUIRE_TRUE = '1';
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%attribute_settings}}';
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['attribute_value', 'attribute_name', 'visible', 'require'], 'string', 'max' => 255],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'             => Yii::t('comments', 'ID'),
            'attribute_value'      => Yii::t('comments', 'Attribute Value'),
            'attribute_name' => Yii::t('comments', 'Attribute Name'),
            'visible'        => Yii::t('comments', 'Visible'),
            'require'        => Yii::t('comments', 'Require'),
        ];
    }
    
    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function searh()
    {
        return AttributeSettings::find()->all();
    }
}