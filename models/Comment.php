<?php
/**
 * Comment.php
 * @author Revin Roman
 * @link https://rmrevin.ru
 */

namespace rmrevin\yii\module\Comments\models;

use rmrevin\yii\module\Comments;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * Class Comment
 * @package rmrevin\yii\module\Comments\models
 *
 * @property integer $id
 * @property string $entity
 * @property string $from
 * @property string $text
 * @property integer $deleted
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property integer $ip
 * @property integer $age
 * @property string $fb_account
 * @property string $tw_account
 * @property string $inst_account
 * @property string $vk_account
 * @property string $title
 * @property string $userphone
 * @property string $email
 * @property string $city
 * @property string $specialty
 * @property string $name
 * @property string $surname
 * @property string $middle_name
 * @property string $imageName
 * @property string $imagePath
 *
 * @property AttributeSettings $commentSettings
 *
 * @property \yii\db\ActiveRecord $author
 * @property \yii\db\ActiveRecord $lastUpdateAuthor
 *
 * @method queries\CommentQuery hasMany(string $class, array $link) see BaseActiveRecord::hasMany() for more info
 * @method queries\CommentQuery hasOne(string $class, array $link) see BaseActiveRecord::hasOne() for more info
 */
class Comment extends \yii\db\ActiveRecord
{
    const NOT_DELETED = 0;
    const DELETED = 1;
    const UPLOAD_PATH = '@frontend/web/uploads/';
    const UPLOAD_DIR = '/uploads/';

    /**
     * @var UploadedFile
     */
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%comment}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\BlameableBehavior::className(),
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            [['text'], 'required'],
            [['from', 'text'], 'string'],
            [['created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['deleted'], 'boolean'],
            [['deleted'], 'default', 'value' => self::NOT_DELETED],
        
            [['ip', 'age'], 'integer'],
            [['fb_account', 'tw_account', 'inst_account', 'vk_account', 'title', 'userphone', 'email', 'city', 'specialty', 'name', 'surname', 'middle_name',], 'string'],
            [['fb_account', 'tw_account', 'inst_account', 'vk_account', 'title', 'userphone', 'email', 'city', 'specialty', 'name', 'surname', 'middle_name',], 'safe' ],
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg, gif'],
        ];
    
        $attributeSettings = AttributeSettings::searh();
        foreach ($attributeSettings as $value) {
            /* @var $value AttributeSettings*/
            if (in_array($value->attribute_value, $this->attributes())) {
                if ($value->require == AttributeSettings::REQUIRE_TRUE) {
                    $rules[] = [[$value->attribute_value], 'required'];
                }
            }
        }
        
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => \Yii::t('comments', 'ID'),
            'entity'     => \Yii::t('comments', 'Entity'),
            'from'       => \Yii::t('comments', 'Comment author'),
            'text'       => \Yii::t('comments', 'Text'),
            'created_by' => \Yii::t('comments', 'Created by'),
            'updated_by' => \Yii::t('comments', 'Updated by'),
            'created_at' => \Yii::t('comments', 'Created at'),
            'updated_at' => \Yii::t('comments', 'Updated at'),

            'ip'           => \Yii::t('comments', 'IP'),
            'age'          => \Yii::t('comments', 'Age'),
            'fb_account'   => \Yii::t('comments', 'Facebook'),
            'tw_account'   => \Yii::t('comments', 'Twitter'),
            'inst_account' => \Yii::t('comments', 'Instagram'),
            'vk_account'   => \Yii::t('comments', 'Vk'),
            'title'        => \Yii::t('comments', 'Title'),
            'userphone'    => \Yii::t('comments', 'Userphone'),
            'email'        => \Yii::t('comments', 'Email'),
            'city'         => \Yii::t('comments', 'City'),
            'specialty'    => \Yii::t('comments', 'Specialty'),
            'name'         => \Yii::t('comments', 'Name'),
            'surname'      => \Yii::t('comments', 'Surname'),
            'middle_name'   => \Yii::t('comments', 'Middle Name'),
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $this->ip = ip2long(\Yii::$app->request->remoteIP);

            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isEdited()
    {
        return $this->created_at !== $this->updated_at;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted === self::DELETED;
    }

    /**
     * @return bool
     */
    public static function canCreate()
    {
        return Comments\Module::instance()->useRbac === true
            ? \Yii::$app->getUser()->can(Comments\Permission::CREATE)
            : true;
    }

    /**
     * @return bool
     */
    public function canUpdate()
    {
        $User = \Yii::$app->getUser();

        return Comments\Module::instance()->useRbac === true
            ? \Yii::$app->getUser()->can(Comments\Permission::UPDATE) || \Yii::$app->getUser()->can(Comments\Permission::UPDATE_OWN,
                ['Comment' => $this])
            : $User->isGuest ? false : $this->created_by === $User->id;
    }

    /**
     * @return bool
     */
    public function canDelete()
    {
        $User = \Yii::$app->getUser();

        return Comments\Module::instance()->useRbac === true
            ? \Yii::$app->getUser()->can(Comments\Permission::DELETE) || \Yii::$app->getUser()->can(Comments\Permission::DELETE_OWN,
                ['Comment' => $this])
            : $User->isGuest ? false : $this->created_by === $User->id;
    }

    /**
     * @return queries\CommentQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Comments\Module::instance()->userIdentityClass, ['id' => 'created_by']);
    }

    /**
     * @return queries\CommentQuery
     */
    public function getLastUpdateAuthor()
    {
        return $this->hasOne(Comments\Module::instance()->userIdentityClass, ['id' => 'updated_by']);
    }

    /**
     * @return object|\yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public static function find()
    {
        return \Yii::createObject(
            Comments\Module::instance()->model('commentQuery'),
            [get_called_class()]
        );
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     * @throws \yii\base\InvalidConfigException
     */
    public function searchComments()
    {
        return Comment::find()->orderBy(['created_at' => SORT_DESC])->all();
    }

    /**
     * @return array|bool
     * @throws \yii\base\Exception
     */
    public function upload()
    {
        if ($this->validate()) {

            $randomString = \Yii::$app->security->generateRandomString(12);
            $uploadDirectory = \Yii::getAlias(self::UPLOAD_PATH) . $randomString . '/';
            $imageName = $randomString . time() . '.' . $this->imageFile->extension;

            if (!is_dir($uploadDirectory)) {
                FileHelper::createDirectory($uploadDirectory);
            }

            if ($this->imageFile->saveAs($uploadDirectory . $imageName)) {
                return [
                    'imagePath' => self::UPLOAD_DIR . $randomString . '/',
                    'imageName' => $imageName,
                ];
            }
        } else {
            return false;
        }

        return true;
    }
}