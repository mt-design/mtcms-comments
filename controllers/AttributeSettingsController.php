<?php

namespace rmrevin\yii\module\Comments\controllers;


use rmrevin\yii\module\Comments\models\AttributeSettings;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class AttributeSettingsController extends Controller
{    
    /**
     * @return string
     */
    public function actionIndex()
    {
        $query = AttributeSettings::find();
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        return $this->render('index', [
            'searchModel'  => new AttributeSettings,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    
    /**
     * Creates a new Adverts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AttributeSettings();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        }
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    /**
     * Updates an existing Adverts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post())) {
            if(!$model->save()){
                print_r($model->errors);die;
            }
            
            return $this->redirect(['index', 'id' => $model->id]);
        }
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAjaxUpdate()
    {
        if (Yii::$app->request->isAjax) {
            $model = $this->findModel(\Yii::$app->request->post('id'));

            if (\Yii::$app->request->post('name') === 'visible') {
                $model->visible = \Yii::$app->request->post('checked') !== 'false' ? '1' : '0';
            }
            if (\Yii::$app->request->post('name') === 'require') {
                $model->require = \Yii::$app->request->post('checked') !== 'false' ? '1' : '0';
            }

            !$model->save() ? $status = false : $status = true;
        }
        return Json::encode(['status' => $status]);
    }
    
    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        
        return $this->redirect(['index']);
    }
    
    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    /**
     * @param $id
     * @return AttributeSettings|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = AttributeSettings::findOne($id)) !== null) {
            return $model;
        }
        
        throw new NotFoundHttpException(Yii::t('common', 'The requested page does not exist.'));
    }
}