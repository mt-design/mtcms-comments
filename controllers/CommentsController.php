<?php

namespace rmrevin\yii\module\Comments\controllers;

use rmrevin\yii\module\Comments\models\Comment;
use rmrevin\yii\module\Comments\models\AttributeSettings;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use rmrevin\yii\module\Comments\models\queries\CommentSearch;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class CommentsController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new CommentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Adverts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     * @throws \Throwable
     */
    public function actionCreate()
    {
        $model = new Comment();

        if ($model->load(Yii::$app->request->post())) {

            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                if (!$imageData = $model->upload()) {
                    throw new \RuntimeException('Upload imageFile error.');
                }

                $model->imageName = $imageData['imageName'];
                $model->imagePath = $imageData['imagePath'];
                $model->imageFile = null;
                if (!$model->save()) {
                    throw new \RuntimeException('Saving $model error.');
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Adverts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $transaction = \Yii::$app->db->beginTransaction();
            try {

                if (!$imageData = $model->upload()) {
                    throw new \RuntimeException('Upload imageFile error.');
                }

                $model->imageName = $imageData['imageName'];
                $model->imagePath = $imageData['imagePath'];
                $model->imageFile = null;
                if (!$model->save()) {
                    throw new \RuntimeException('Saving $model error.');
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @param $id
     * @return Comment|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Comment::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('common', 'The requested page does not exist.'));
    }
    
    /**
     * @return string
     */
    public function actionSettings()
    {
        $query = AttributeSettings::find();
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        return $this->render('settings', [
            'searchModel'  => new AttributeSettings,
            'dataProvider' => $dataProvider,
        ]);
    }
}