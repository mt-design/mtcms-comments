<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model \rmrevin\yii\module\Comments\models\AttributeSettings */

$this->title = Yii::t('common', 'Create Attribute');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Attribute Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
