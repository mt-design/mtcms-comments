<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \rmrevin\yii\module\Comments\models\AttributeSettings */

$this->title = Yii::t('comments', 'Update Attribute: ' . $model->attribute_value, [
    'nameAttribute' => '' . $model->attribute_value,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('comments', 'Attribute Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->attribute_value, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('comments', 'Update');
?>
<div class="comment-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
