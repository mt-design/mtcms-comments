<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel \rmrevin\yii\module\Comments\models\AttributeSettings */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'AttributeSettings');
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="attribute-settings-index">

        <?php Pjax::begin([]); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a(Yii::t('common', 'Create Attribute'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'id'           => 'pjaxGrid',
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                'id',
                'attribute_value',
                'attribute_name',
                [
                    'class'           => 'yii\grid\CheckboxColumn',
                    'header'          => 'Обязательное',
                    'checkboxOptions' => function ($model) {
                        return $model->visible ? ['checked' => "checked"] : [];
                    },
                    'contentOptions'  => [
                        'data-name' => 'visible',
                    ]
                ],
                [
                    'header'          => 'Обязательное',
                    'class'           => 'yii\grid\CheckboxColumn',
                    'checkboxOptions' => function ($model) {
                        return $model->require ? ['checked' => "checked"] : [];
                    },
                    'contentOptions'  => [
                        'data-name' => 'require',
                    ]
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>

<?php
$script = <<<JS

$("input[type=checkbox]").on('click',function(){
  
    var id = $(this).closest('tr').data().key,
    checked = $(this).is(':checked'),
    name = $(this).parent().data().name;
    
    $.ajax({
        url: '/admin/attribute-settings/ajax-update',
        type: 'post',
        data: {id: id, checked: checked, name: name},
        success: function(data){
          $.pjax.reload({container:"#pjaxGrid", push : false,});
          console.log(data);
        }
    })
});

JS;
$this->registerJs($script, $this::POS_READY);
?>