<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \rmrevin\yii\module\Comments\models\AttributeSettings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="attribute-settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'attribute_value')->textInput() ?>

    <?= $form->field($model, 'attribute_name')->textInput() ?>

    <?= $form->field($model, 'visible')->checkbox() ?>

    <?= $form->field($model, 'require')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('comments', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
