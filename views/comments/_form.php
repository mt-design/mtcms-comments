<?php

use rmrevin\yii\module\Comments\models\AttributeSettings;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \rmrevin\yii\module\Comments\models\Comment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comment-form">
    
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    
    <?= $form->field($model, 'from')->textInput() ?>
    
    <?= $form->field($model, 'text')->textarea(['row' => 8]) ?>
    
    <?php
    $attributeSettings = AttributeSettings::searh();
    foreach ($attributeSettings as $value) {
        if (in_array($value->attribute_value, $model->attributes())) {
            if ($value->visible == AttributeSettings::VISIBLE_TRUE) {
                echo $form->field($model, $value->attribute_value)->textInput();
            }
        }
    }
    ?>
    
    <?= $form->field($model, 'entity')->textInput() ?>

    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('comments', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>
