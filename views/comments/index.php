<?php

use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel \app\modules\admin\models\CommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Comments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comments-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('common', 'Create Comment'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                'id',
                'text',
                'email:email',
                'name',
                'entity',
                'created_at:dateTime',
                [
                    'label' => 'Ip',
                    'value' => function ($data) {
                        return long2ip($data->ip);
                    }
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?></div>
    <?php Pjax::end(); ?>
</div>
