<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel \rmrevin\yii\module\Comments\models\AttributeSettings */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index">
    
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('common', 'Create Categories'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'attribute',
            'attribute_name',
            'visible',
            'require',
            
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
