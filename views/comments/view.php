<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \rmrevin\yii\module\Comments\models\Comment*/

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Comments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-view">

    <h4><?= Html::encode($this->title) ?></h4>

    <p>
        <?= Html::a(Yii::t('common', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method'  => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'title',
            'text',
            'name',
            'surname',
            'middle_name',
            'email',
            'userphone',
            'specialty',
            'city',
            'age',
            'fb_account',
            'tw_account',
            'inst_account',
            'vk_account',
            'entity',
            'from',
            'created_by',
            'updated_by',
            'created_at:dateTime',
            'updated_at:dateTime',
            [
                'label' => 'Ip',
                'value' => function ($data) {
                    return long2ip($data->ip);
                }
            ],
        ],
    ]) ?>

</div>
