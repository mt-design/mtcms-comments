<?php
/**
 * comment-form.php
 * @author Revin Roman
 * @link https://rmrevin.ru
 *
 * @var yii\web\View $this
 * @var Comments\forms\CommentCreateForm $CommentCreateForm
 */

use rmrevin\yii\module\Comments;
use yii\helpers\Html;

/** @var Comments\widgets\CommentFormWidget $Widget */
$Widget = $this->context;

?>

<a name="commentcreateform"></a>
<div class="row comment-form">
    <div class="col-md-12">
        <?php
        /** @var \yii\widgets\ActiveForm $form */
        $form = \yii\widgets\ActiveForm::begin();
        
        echo Html::activeHiddenInput($CommentCreateForm, 'id');
        
        if (\Yii::$app->getUser()->getIsGuest()) {
            echo $form->field($CommentCreateForm, 'from')
                ->textInput();
        }
        
        $options = [];
        if ($Widget->Comment->isNewRecord) {
            $options['data-role'] = 'new-comment';
        }
        
        echo $form->field($CommentCreateForm, 'title')->textInput();
        
        echo $form->field($CommentCreateForm, 'text')->textarea($options);

        echo $form->field($CommentCreateForm, 'name')->textInput();
        
        echo $form->field($CommentCreateForm, 'surname')->textInput();
        
        echo $form->field($CommentCreateForm, 'middle_name')->textInput();

        echo $form->field($CommentCreateForm, 'age')->textInput();

        echo $form->field($CommentCreateForm, 'email')->textInput();

        echo $form->field($CommentCreateForm, 'userphone')->textInput();

        echo $form->field($CommentCreateForm, 'specialty')->textInput();

        echo $form->field($CommentCreateForm, 'city')->textInput();

        echo $form->field($CommentCreateForm, 'fb_account')->textInput();

        echo $form->field($CommentCreateForm, 'tw_account')->textInput();

        echo $form->field($CommentCreateForm, 'inst_account')->textInput();

        echo $form->field($CommentCreateForm, 'vk_account')->textInput();
        
        ?>
        <div class="actions">
            <?= Html::submitButton(\Yii::t('comments', 'Post comment'), [
                'class' => 'btn btn-primary',
            ]) ?>
            <?= Html::resetButton(\Yii::t('comments', 'Cancel'), [
                'class' => 'btn btn-link',
            ]) ?>
        </div>
        <?php
        \yii\widgets\ActiveForm::end();
        ?>
    </div>
</div>