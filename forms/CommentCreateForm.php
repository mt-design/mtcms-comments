<?php
/**
 * CommentCreateForm.php
 * @author Revin Roman
 * @link https://rmrevin.ru
 */

namespace rmrevin\yii\module\Comments\forms;

use rmrevin\yii\module\Comments;
use rmrevin\yii\module\Comments\models\AttributeSettings;

/**
 * Class CommentCreateForm
 * @package rmrevin\yii\module\Comments\forms
 */
class CommentCreateForm extends \yii\base\Model
{
    public $id;
    public $entity;
    public $from;
    public $text;
    
    public $ip;
    public $age;
    public $fb_account;
    public $tw_account;
    public $inst_account;
    public $vk_account;
    public $title;
    public $userphone;
    public $email;
    public $city;
    public $specialty;
    public $name;
    public $surname;
    public $middle_name;
    
    /** @var Comments\models\Comment */
    public $Comment;
    
    public function init()
    {
        $Comment = $this->Comment;
        
        if (false === $this->Comment->isNewRecord) {
            $this->id = $Comment->id;
            $this->entity = $Comment->entity;
            $this->from = $Comment->from;
            $this->text = $Comment->text;
            
            $this->ip = $Comment->ip;
            $this->age = $Comment->age;
            $this->fb_account = $Comment->fb_account;
            $this->tw_account = $Comment->tw_account;
            $this->inst_account = $Comment->inst_account;
            $this->vk_account = $Comment->vk_account;
            $this->title = $Comment->title;
            $this->userphone = $Comment->userphone;
            $this->email = $Comment->email;
            $this->city = $Comment->city;
            $this->specialty = $Comment->specialty;
            $this->name = $Comment->name;
            $this->surname = $Comment->surname;
            $this->middle_name = $Comment->middle_name;
            
        } elseif (!\Yii::$app->getUser()->getIsGuest()) {
            $User = \Yii::$app->getUser()->getIdentity();
            
            $this->from = $User instanceof Comments\interfaces\CommentatorInterface
                ? $User->getCommentatorName()
                : null;
        }
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $CommentModelClassName = Comments\Module::instance()->model('comment');
        
       $rules = [
            [['entity', 'text'], 'required'],
            [['entity', 'from', 'text'], 'string'],
            [['id'], 'integer'],
            [['id'], 'exist', 'targetClass' => $CommentModelClassName, 'targetAttribute' => 'id'],

            [['ip', 'age'], 'integer'],
            [['fb_account', 'tw_account', 'inst_account', 'vk_account', 'title', 'userphone', 'email', 'city', 'specialty', 'name', 'surname', 'middle_name',], 'string'],
            [['fb_account', 'tw_account', 'inst_account', 'vk_account', 'title', 'userphone', 'email', 'city', 'specialty', 'name', 'surname', 'middle_name',], 'safe' ],
        ];

        $attributeSettings = AttributeSettings::searh();
        foreach ($attributeSettings as $value) {
            /* @var $value AttributeSettings*/
            if (in_array($value->attribute_value, $this->attributes())) {
                if ($value->require == AttributeSettings::REQUIRE_TRUE) {
                    $rules[] = [[$value->attribute_value], 'required'];
                }
            }
        }

        return $rules;
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'entity' => \Yii::t('comments', 'Entity'),
            'from'   => \Yii::t('comments', 'Your name'),
            'text'   => \Yii::t('comments', 'Text'),
            
            'ip'           => \Yii::t('comments', 'IP'),
            'age'          => \Yii::t('comments', 'Age'),
            'fb_account'   => \Yii::t('comments', 'Facebook'),
            'tw_account'   => \Yii::t('comments', 'Twitter'),
            'inst_account' => \Yii::t('comments', 'Instagram'),
            'vk_account'   => \Yii::t('comments', 'Vk'),
            'title'        => \Yii::t('comments', 'Title'),
            'userphone'    => \Yii::t('comments', 'Userphone'),
            'email'        => \Yii::t('comments', 'Email'),
            'city'         => \Yii::t('comments', 'City'),
            'specialty'    => \Yii::t('comments', 'Specialty'),
            'name'         => \Yii::t('comments', 'Name'),
            'surname'      => \Yii::t('comments', 'Surname'),
            'middle_name'  => \Yii::t('comments', 'Middle Name'),
        ];
    }
    
    /**
     * @return bool
     * @throws \yii\web\NotFoundHttpException
     */
    public function save()
    {
        $Comment = $this->Comment;
        
        $CommentModelClassName = Comments\Module::instance()->model('comment');
        
        if (empty($this->id)) {
            $Comment = \Yii::createObject($CommentModelClassName);
        } elseif ($this->id > 0 && $Comment->id !== $this->id) {
            /** @var Comments\models\Comment $CommentModel */
            $CommentModel = \Yii::createObject($CommentModelClassName);
            $Comment = $CommentModel::find()
                ->byId($this->id)
                ->one();
            
            if (!($Comment instanceof Comments\models\Comment)) {
                throw new \yii\web\NotFoundHttpException;
            }
        }
        
        $Comment->entity = $this->entity;
        $Comment->from = $this->from;
        $Comment->text = $this->text;
    
        $Comment->ip = $this->ip;
        $Comment->age = $this->age;
        $Comment->fb_account = $this->fb_account;
        $Comment->tw_account = $this->tw_account;
        $Comment->inst_account = $this->inst_account;
        $Comment->vk_account = $this->vk_account;
        $Comment->title = $this->title;
        $Comment->userphone = $this->userphone;
        $Comment->email = $this->email;
        $Comment->city = $this->city;
        $Comment->specialty = $this->specialty;
        $Comment->name = $this->name;
        $Comment->surname = $this->surname;
        $Comment->middle_name = $this->middle_name;
        
        $result = $Comment->save();
        
        if ($Comment->hasErrors()) {
            foreach ($Comment->getErrors() as $attribute => $messages) {
                foreach ($messages as $mes) {
                    $this->addError($attribute, $mes);
                }
            }
        }
        
        $this->Comment = $Comment;
        
        return $result;
    }
}